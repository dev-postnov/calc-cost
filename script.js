    Vue.config.devtools = true;

       var app = new Vue({
         el: '#app',
         data: {
           title: '',
           describe: '',
           products: [
             // {
             //   name: '',
             //   cost: '',
             //   value: '',
             //   valuePerClient: '',
             //   costPerClient: 0
             // }
           ],
           finalCostPerClient: ''
         },
         updated() {
         	 this.countFinalCostPerClient();
         },
         computed: {
          // Подсчет количества элементов
           totalCount() { return this.products.length; }
         },

         methods: {

          // Удаление элемента, расчет стоимости
           removeItem(index) {
           	console.log(index);
           	this.products = this.products.filter((item, i) => i !== index);
           },

           // Добавление элемента
           addItem(event) {
             event.preventDefault();
             this.products.push(
               {
                 name: '',
                 cost: '',
                 value: '',
                 valuePerClient: '',
                 costPerClient: 0
               }
             );
           },

           // Сброс данных в элементе
           resetData(item) {
           	  item.name= '';
              item.cost = '';
              item.value = '';
              item.valuePerClient = '';
              item.costPerClient = 0;
           },

           //Подсчет суммы в элементе
           countCostPerClient(item) {
             if (item.cost != false && item.value != false && item.valuePerClient != false) {
               item.costPerClient = (item.cost / item.value * item.valuePerClient).toFixed(2);
             }
           },

           //Подсчет итоговой суммы на одного клиента
           countFinalCostPerClient() {
             sum = 0;
             this.products.forEach((item) => {
             	sum += +item.costPerClient;
             });
             this.finalCostPerClient = sum;
           }
         }
       });
